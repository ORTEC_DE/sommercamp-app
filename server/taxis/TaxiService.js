var Taxi = require('./Taxi');
var LatLng = require('./LatLng');

class TaxiService {

    constructor(networkHandler) {
        // erstellt ein Array mit 3 Taxi-Instanzen
        this.taxis = [
            new Taxi('Eins', new LatLng(49.4, 8.7), networkHandler.update),
            new Taxi('Zwei', new LatLng(49.45, 8.75), networkHandler.update),
            new Taxi('Drei', new LatLng(49.35, 8.65), networkHandler.update),
        ];
    }

    /**
     * Aktualisiert das Taxi-Array
     * @param taxis
     */
    setTaxis(taxis) {
        this.taxis = taxis;
    }

    /**
     * Gibt das Taxi-Array zurück
     * @returns {Taxi[]}
     */
    getTaxis() {
        return this.taxis;
    }

    /**
     * Wird aufgerufen wenn ein Taxi gebucht werden soll.
     *
     * @param start Startpunkt als LatLng
     * @param destination Zielpunkt als LatLng
     * @returns Taxi Gibt eine Taxi-Instanz zurück
     */
    bookTaxi(start, destination) {
        var shortestDistance = null;
        var selectedTaxi = null;

        for (var i = 0; i < this.taxis.length; i++) {
            var taxi = this.taxis[i];

            if (taxi.state !== Taxi.States.FREE) {
                continue;
            }

            var currentDistanceToStartPosition = Math.sqrt(Math.pow(taxi.position.lat - start.lat, 2) + Math.pow(taxi.position.lng - start.lng, 2));

            if ((shortestDistance === null) || (shortestDistance > currentDistanceToStartPosition)) {
                shortestDistance = currentDistanceToStartPosition;
                selectedTaxi = taxi;
            }
        }

        if (selectedTaxi) {
            selectedTaxi.allocate(start, destination);
        }

        return selectedTaxi;
    }

}


module.exports = TaxiService;