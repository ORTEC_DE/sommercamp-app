var LatLng = require('./LatLng');

var TaxiState = {
    FREE: 'free',
    ALLOCATED: 'allocated',
};

class Taxi {

    /**
     * Wird ausgeführt, wenn eine neue Taxi-Instanz erzeugt wird.
     * Für eine Instanz wird ein Name, eine Position, sowie ein Update-Callback benötigt
     *
     * @param name Name der Instanz
     * @param position Position der Instanz
     * @param updateCallback Update-Callback: Soll aufgerufen werden, wenn sich der Zustand dieser Instanz geändert hat.
     */
    constructor(name, position, updateCallback) {
        // speichere den Namen des Taxis in der Instanz der Klasse
        this.name = name;

        // setze den Status des Taxis standardmäßig auf frei
        this.state = TaxiState.FREE;

        // setze die Position des Taxis entweder auf den beim Erstellen übergenen Wert oder falls dieser 'null' oder 'undefined' ist auf {lat:0, lng: 0}
        this.position = position || LatLng.Zero;

        // initialisiere die Zielliste als leere Liste.
        this.destinations = [];

        // überprüfe ob das übergebene Update-Callback eine Funktion ist
        if (typeof updateCallback !== 'function') {
            // falls ja: werfe einen Fehler
            throw new Error('The update callback of the Taxi-Constructor is mandatory.');
        }

        // speichere das Update-Callback in der Instanz der Klasse mit dem Namen 'onUpdate'
        // das Update-Callback lässt sich nun mit 'this.onUpdate(taxiInstanz)' aufrufen.
        this.onUpdate = updateCallback;

        // bind functions to this context
        this.update = this.update.bind(this);

        // startet den Updatezyklus des Taxis. Die update-Methode wird ab jetzt alle 1000 Milisekunden (= 1 Sekunde) aufgerufen.
        setInterval(this.update, 1000);
    }

    /**
     * Diese Methode reserviert das Taxi für eine bestimmte Fahrt.
     * @param start {LatLng} Startpunkt der Fahrt
     * @param destination {LatLng} Ziel der Fahrt
     */
    allocate(start, destination) {
        // ersetze alle vorherigen Ziele mit dem Start und dem Ziel der gebuchten Reise
        this.destinations = [start, destination];

        // setze den Status des Taxis auf belegt
        this.state = TaxiState.ALLOCATED;
    }

    /**
     * Diese Methode wird sekündlich aufgerufen und aktualisiert den Status des Taxis
     */
    update() {
        if (this.state === TaxiState.FREE && this.destinations.length === 0) {
            // Wenn dieses Taxi zurzeit frei ist und es keine festen Ziele gibt...
            if (Math.random() > 0.9) {
                // ... und wenn die Zufallszahl zwischen 0 und 1 größer als 0.9 ist,
                // dann erstelle zwei zufällige Werte zwischen -0,01 und 0,01
                var deltaLat = (Math.random() - 0.5) / 50;
                var deltaLng = (Math.random() - 0.5) / 50;

                // Diese werden jeweils auf die Latitude oder Longitude addiert.
                var nextLat = this.position.lat + deltaLat;
                var nextLng = this.position.lng + deltaLng;

                // Aus den neuen Werten wird ein neues LatLng-Objekt erstellt und in als Ziel aufgenommen.
                this.destinations.push(new LatLng(nextLat, nextLng));
            }
        }

        if (this.destinations.length > 0) {
            // wenn es mehr als 0 Ziele gibt, dann drive()
            this.drive();

        }

        // Update dieses Taxi
        this.onUpdate(this);
    }

    /**
     * Diese Methode implenmentiert die Fahrroutine des Taxis.
     */
    drive() {
        if (this.destinations.length > 0) {
            // wenn die Zielliste mindestens ein Ziel enhält,
            // dann speichere das erste Ziel in der Variable 'destination'.
            var destination = this.destinations[0];

            if (typeof destination.stepLengthLat === 'undefined') {
                // wenn das Ziel noch keine Schrittlänge für die Latitude enthält,
                // dann berechne eine Schrittlänge, indem die Differenz von Ziel und aktueller Position durch 10 geteilt wird.
                // Das heißt in 10 Schritten (Updatezyklen des Taxis) ist das Ziel erreicht
                destination.stepLengthLat = (destination.lat - this.position.lat) / 10;
            }

            if (typeof destination.stepLengthLng === 'undefined') {
                // verfahre analog zur Latitude
                destination.stepLengthLng = (destination.lng - this.position.lng) / 10;
            }

            // berechne die Toleranz die bei der Überprüfung verwendet wird, ob das Taxi das Ziel ereicht hat.
            // Dies ist nötig, da beim Rechnen mit Kommazahlen ungenauigkeiten auftreten können, wenn Nachkommastellen wegen des Speichers abgeschnitten werden:
            // Beispiel: 2 / 3 = 0.6666666666666666 aber eben nicht 0.6 Periode.
            var tolerance = (Math.abs(destination.stepLengthLat) + Math.abs(destination.stepLengthLng)) / 10;

            // kalkuliere ob die Position des Taxis mit dem Ziel inkl Toleranz übereinstimmt,
            // wenn ja, dann setze die jeweilige Schrittvariable auf 0.
            if (
                (destination.stepLengthLat < 0 && this.position.lat <= destination.lat + tolerance) ||
                (destination.stepLengthLat > 0 && this.position.lat >= destination.lat - tolerance)
            ) {
                destination.stepLengthLat = 0;
            }

            if (
                (destination.stepLengthLng < 0 && this.position.lng <= destination.lng + tolerance) ||
                (destination.stepLengthLng > 0 && this.position.lng >= destination.lng - tolerance)
            ) {
                destination.stepLengthLng = 0;
            }

            // Addiere die jeweils positive oder negative Schrittlänge auf die aktuelle Position des Taxis
            this.position.lat += destination.stepLengthLat;
            this.position.lng += destination.stepLengthLng;

            if (destination.stepLengthLat === 0 && destination.stepLengthLng === 0) {
                // wenn beide Schrittlängen-Variablen des Ziels den Wert 0 haben,
                // dann entferne das erste Ziel (this.destinations[0]) aus der Zielliste
                this.destinations.shift();

                if (this.destinations.length === 0) {
                    // wenn die Zielliste leer ist, dann setze den Status des Taxis auf 'frei'.
                    this.state = TaxiState.FREE;
                }
            }
        }
    }
}

Taxi.States = TaxiState;

module.exports = Taxi;