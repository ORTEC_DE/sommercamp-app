var LatLng = require('./LatLng');
var Taxi = require('./Taxi');
var TaxiService = require('./TaxiService');

class TaxiNetworkHandler {

    /**
     * Erstellt eine neue TaxiNetworkHandler-Instanz.
     * Der TaxiNetworkHandler kümmert sich um die Netzwerkkommunikation und leitet entsprechende Requests an den TaxiService weiter.
     *
     * @param app Die Express.js App-Instanz
     * @param io Die Socket.IO-Instanz
     */
    constructor(app, io) {
        // binde alle Eventhandler an diese Instanz (this).
        this.handleGetTaxis = this.handleGetTaxis.bind(this);
        this.handleBookTaxi = this.handleBookTaxi.bind(this);
        this.update = this.update.bind(this);

        // erstelle einen neuen TaiService und speichere die Instanz in der Instanzvariable 'service'
        this.service = new TaxiService(this);

        // erstelle einen neuen Socket.IO-Namespace und speichere die Instanz in der Instanzvariable 'socket'
        this.socket = io.of('/taxis');

        // leite alle HTTP-GET-Requests an die handleGetTaxis-Funktion weiter
        app.get('/api/taxis', this.handleGetTaxis);

        // leite alle HTTP-POST-Requests an die handleBookTaxi-Funktion weiter
        app.post('/api/taxis', this.handleBookTaxi);
    }

    /**
     * Diese Funktion dient zum Abrufen aller Taxis.
     * Wird gerufen wenn ein Client via HTTP an '/api/taxis' einen GET-Request macht.
     *
     * @param req Express.js Request
     * @param res Express.js Response
     */
    handleGetTaxis(req, res) {
        // hole alle Taxi-Instanzen aus dem TaxiService.
        var taxis = this.service.getTaxis();

        // wandle das Taxi-Array in ein Objekt um in dem alle Taxis mit Namen indiziert sind:
        var taxisByName = taxis.reduce(function (taxisByName, taxi, index) {
            taxisByName[taxi.name] = taxi;

            return taxisByName;
        }, {});

        // schreibe die Taxis als JSON in die Antwort
        res.json(taxisByName);
    }

    /**
     * Diese Funktion dient zum Buchen eines Taxis.
     * Wird gerufen wenn ein Client via HTTP an '/api/taxis' einen POST-Request macht.
     *
     * @param req Express.js Request
     * @param res Express.js Response
     */
    handleBookTaxi(req, res) {
        // extrahiere 'start' und 'destination' aus dem Request-Body
        var start = req.body.start;
        var destination = req.body.destination;

        // überprüfe, ob start ein Objekt ist, dass die Zahlen lat und lng enthält
        if (typeof start !== 'object' || typeof start.lat !== 'number' || typeof start.lng !== 'number' ) {
            return res.end('"start" should be an object, containing a numeric "lat" and "lng" property');
        }

        // überprüfe, ob start ein Objekt ist, dass die Zahlen lat und lng enthält
        if (typeof destination !== 'object' || typeof destination.lat !== 'number' || typeof destination.lng !== 'number' ) {
            return res.end('"destination" should be an object, containing a numeric "lat" and "lng" property');
        }

        // konvertiere start und destination zu LatLng-Instanzen
        start = new LatLng(start.lat, start.lng);
        destination = new LatLng(destination.lat, destination.lng);

        // Buche ein Taxi über den TaxiService
        var taxi = this.service.bookTaxi(start, destination);

        // wenn der TaxiService einen Wert der eine Instanz der Taxiklasse ist zurückgibt
        if (taxi instanceof Taxi) {
            // dann rufe die update Methode dieser Instanz
            this.update(taxi);

            // schreibe das Taxi also JSON in die Antwort
            return res.json({
                taxi: taxi,
            });
        } else {
            // wenn keine Taxi-Instanz zurück gegeben wird, ist etwas nicht nach Plan gelaufen:
            // antworte dem Client mit einem Fehler

            res.status(400).json({
                error: taxi,
            });
        }
    }

    /**
     * Teilt eine Änderung einer Taxi-Instanz allen zurzeit bekannten Clients mit.
     *
     * @param taxi Die Taxi-Instanz
     */
    update(taxi) {
        if (taxi instanceof Taxi) {
            this.socket.emit('taxi.updated', {
                taxi: taxi,
            });
        } else {
            console.warn('Ungültiger Wert an Taxi-Update-Methode übergeben:', taxi);
        }
    }
}


module.exports = TaxiNetworkHandler;