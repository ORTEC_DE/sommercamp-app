class LatLng {

    constructor(lat, lng) {
        this.lat = lat;
        this.lng = lng;
    }

}

LatLng.Zero = new LatLng(0, 0);

module.exports = LatLng;