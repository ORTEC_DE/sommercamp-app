var fs = require('fs');
var path = require('path');
var http = require('http');
var https = require('https');
var express = require('express');
var bodyParser = require('body-parser');
var Socket = require('socket.io');

var TaxiNetworkHandler = require('./taxis/TaxiNetworkHandler');

var httpPort = 8080;
var httpsPort = 8443;
var httpsOptions = {
    passphrase: 'Ortec',
    key: fs.readFileSync(path.join(__dirname, 'certificate', 'key.pem')),
    cert: fs.readFileSync(path.join(__dirname, 'certificate', 'certificate.pem')),
};

// erstelle eine neue Express-App-Instanz
var app = express();

// erstelle ein HTTP-Server-Instanz
var httpServer = http.createServer(app);
var httpsServer = https.createServer(httpsOptions, app);

// erstelle eine neue Socket-Instanz, zur Echtzeitkommunikation
var io = new Socket();

io.attach(httpServer);
io.attach(httpsServer);

// Füge die BodyParser Express-Middleware ein, um einfachen Zugriff auf den Body von JSON-Requestes zu erhalten
app.use(bodyParser.json());

// liefere den Ordner '../webapp' aus
var webappPath = path.join(__dirname, '..', 'webapp');
app.use('/', express.static(webappPath));


// starte den TaxiNetworkHandler
var taxiHandler = new TaxiNetworkHandler(app, io);


// starte den Server auf dem angegebenen Port
httpServer.listen(httpPort, '0.0.0.0', () => {
    console.log('HTTP-Server lauscht auf Port: ' + httpPort);
});

httpsServer.listen(httpsPort, '0.0.0.0', () => {
    console.log('HTTPS-Server lauscht auf Port: ' + httpsPort);
});
