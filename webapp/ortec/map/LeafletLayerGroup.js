


sap.ui.define([
    'jquery.sap.global',
    './LeafletLayer',
], function(jQuery, LeafletLayer) {
    'use strict';

    var LeafletLayerGroup = LeafletLayer.extend('ortec.map.LeafletLayerGroup', {
        metadata: {
            library: 'ortec.map',
            properties: {
                detachOnRerender: { type: 'boolean', defaultValue: true },
            },
            aggregations: {
                children: { type: 'ortec.map.LeafletLayer', singularName: 'child', multiple: true, bindable: 'bindable' },
            },
            defaultAggregation: 'children',
        },

        constructor: function() {
            // call base-constructor
            LeafletLayer.apply(this, arguments);

        },

        init: function() {
            LeafletLayer.prototype.init && LeafletLayer.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            jQuery.sap.log.info('LeafletLayerGroup initialized');
        },

        destroy: function() {
            LeafletLayer.prototype.destroy && LeafletLayer.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletLayerGroup destroyed');
        },

        exit: function() {
            LeafletLayer.prototype.exit && LeafletLayer.prototype.exit.apply(this, arguments);

            this.detach();

            jQuery.sap.log.info('LeafletLayerGroup exited');
        },

        rerenderLayerObject: function() {
            var bDetachOnRerender = this.getDetachOnRerender();

            bDetachOnRerender && this.detach();

            var oLayerGroup = this.getLayer();

            if (!oLayerGroup) {
                oLayerGroup = L.layerGroup();

                this.setProperty('layer', oLayerGroup, true);
            } else {
                // oLayerGroup.clearLayers()
            }

            var aUnprocessedLayers = oLayerGroup.getLayers();

            var aChildren = this.getChildren();

            for (var i = 0; i < aChildren.length; i++) {
                var oChild = aChildren[i];
                var oLayerObject;

                if (oChild.rerenderLayerObject && typeof oChild.rerenderLayerObject === 'function') {
                    oLayerObject = oChild.rerenderLayerObject();
                } else {
                    oLayerObject = oChild.getLayer();
                }

                var iLayerObjectIndex = aUnprocessedLayers.indexOf(oLayerObject);

                if (iLayerObjectIndex >= 0) {
                    aUnprocessedLayers.splice(iLayerObjectIndex, 1);
                } else if (oLayerObject) {
                    oLayerGroup.addLayer(oLayerObject);
                    oChild.setAttachedTo(oLayerGroup);

                    jQuery.sap.log.info('Layer added to Layergroup, key: ' + oChild.getKey());
                }
            }

            for (var i = 0; i < aUnprocessedLayers.length; i++) {
                var oUnprocessedChild = aUnprocessedLayers[i];

                oLayerGroup.removeLayer(oUnprocessedChild);
            }

            return oLayerGroup;
        },
    });



    return LeafletLayerGroup;
});