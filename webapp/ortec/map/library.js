sap.ui.define([
    'jquery.sap.global',
    './third-party/leaflet/leaflet',
], function(jQuery) {

    'use strict';

    /**
     * @alias ortec.lbvis
     */
    sap.ui.getCore().initLibrary({
        name: 'ortec.map',
        version: '1.0.0',
        dependencies: [
            'sap.ui.core'
        ],
        types: [],
        interfaces: [],
        controls: [
            'ortec.map.LeafletControl',
            'ortec.map.MapLayerControl',
        ],
        elements: [
            'ortec.map.AwesomeIcon',
            'ortec.map.LeafletIcon',
            'ortec.map.LeafletLayer',
            'ortec.map.LeafletLayerGroup',
            'ortec.map.LeafletMarkerBase',
            'ortec.map.LeafletTileLayer',
        ],
        extensions: []
    });

    // export library
    return ortec.map;

});