sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/Element',
], function(jQuery, Element) {
    'use strict';

    var LeafletTileLayer = Element.extend('ortec.map.LeafletTileLayer', {
        metadata: {
            library: 'ortec.map',
            properties: {
                name: { type: 'string', defaultValue: null },

                tileSource: { type: 'string', defaultValue: null },
                tileErrorSource: { type: 'string', defaultValue: '' },  // URL to the tile image to show in place of the tile that failed to load.
                attribution: { type: 'string', defaultValue: '' },      // String to be shown in the attribution control, describes the layer data, e.g. "© Mapbox".

                subdomains: { type: 'string[]', defaultValue: [] },     // Subdomains of the tile service.

                minZoom: { type: 'int', defaultValue: 0 },              // The minimum zoom level down to which this layer will be displayed (inclusive).
                maxZoom: { type: 'int', defaultValue: 18 },             // The maximum zoom level up to which this layer will be displayed (inclusive).
                zoomOffset: { type: 'int', defaultValue: 0 },           // The zoom number used in tile URLs will be offset with this value.
                zoomReverse: { type: 'boolean', defaultValue: false },  // If set to true, the zoom number used in tile URLs will be reversed (maxZoom - zoom instead of zoom)

                tms: { type: 'boolean', defaultValue: false },          // If true, inverses Y axis numbering for tiles (turn this on for TMS services).
                detectRetina: { type: 'boolean', defaultValue: false }, // If true and user is on a retina display, it will request four tiles of half the specified size and a bigger zoom level in place of one to utilize the high resolution.
                crossOrigin: { type: 'boolean', defaultValue: false },  // If true, all tiles will have their crossOrigin attribute set to ''. This is needed if you want to access tile pixel data.

                tileSize: { type: 'int', defaultValue: 256 },           // Width and height of tiles in the grid. Use a number if width and height are equal, or L.point(width, height) otherwise.
                opacity: { type: 'float', defaultValue: 1.0 },          // Opacity of the tiles. Can be used in the createTile() function.

                updateWhenZooming: { type: 'boolean', defaultValue: true }, // By default, a smooth zoom animation (during a touch zoom or a flyTo()) will update grid layers every integer zoom level. Setting this option to false will update the grid layer only when the smooth animation ends.
                updateInterval: { type: 'int', defaultValue: 200 },     // Tiles will not update more than once every updateInterval milliseconds when panning.
            },
        },

        constructor: function() {
            // call base-constructor
            Element.apply(this, arguments);

        },

        init: function() {
            Element.prototype.init && Element.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            jQuery.sap.log.info('LeafletTileLayer initialized');
        },

        destroy: function() {
            Element.prototype.destroy && Element.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletTileLayer destroyed');
        },

        exit: function() {
            Element.prototype.exit && Element.prototype.exit.apply(this, arguments);

            jQuery.sap.log.info('LeafletTileLayer exited');
        },

        getOptionsObject: function () {
            var oMetadata = this.getMetadata();
            var oProperties = oMetadata.getProperties();
            var oOptions = {};

            for (var oProperty in oProperties) {
                if (oProperties.hasOwnProperty(oProperty)) {
                    switch (oProperty) {
                        case 'name':
                        case 'tileSource': {
                            continue;
                        }

                        default: {
                            oOptions[oProperty] = this.getProperty(oProperty);
                        }
                    }
                }
            }

            return oOptions;
        }

    });



    return LeafletTileLayer;
});