sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/Element',
], function(jQuery, Element) {
    'use strict';

    var LeafletLayer = Element.extend('ortec.map.LeafletLayer', {
        metadata: {
            library: 'ortec.map',
            abstract: true,
            properties: {
                key: { type: 'string', defaultValue: null },

                layer: { type: 'object' },
            },
        },

        _oAttachedTo: undefined,

        init: function() {
            Element.prototype.init && Element.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            jQuery.sap.log.info('LeafletLayer initialized');
        },

        destroy: function() {
            Element.prototype.destroy && Element.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletLayer destroyed');
        },

        exit: function() {
            Element.prototype.exit && Element.prototype.exit.apply(this, arguments);

            this.detach();

            jQuery.sap.log.info('LeafletLayer exited');
        },

        getAttachedTo: function () {
            return this._oAttachedTo;
        },

        setAttachedTo: function (oAttachedTo) {
            this._oAttachedTo = oAttachedTo;
        },

        /*
         *  "renderers the Leaflet Layer object. In this class the implementation is  more or less a dummy - just returning the layer property
         *  In subclasses which render leaflet layers on the fly it can be overloaded.
         */
        rerenderLayerObject: function() {
            return this.getLayer();
        },

        attachToMapObject: function(oMapObject) {
            var oLayer = this.rerenderLayerObject();

            if (oLayer) {
                if (!oMapObject.hasLayer(oLayer)) {
                    oLayer.addTo(oMapObject);
                }

                this.setAttachedTo(oMapObject);

                jQuery.sap.log.info('Layer added to map, key: ' + this.getKey());
            }
        },

        detach: function() {
            var oLayer = this.getLayer();
            var oMapObj = this.getAttachedTo();

            if (oLayer && oMapObj) {
                oMapObj.removeLayer(oLayer);
                this.setAttachedTo(null);
            }
        },

        setLayer: function(oLayer) {
            this.detach();
            this.setProperty('layer', oLayer);
        },

    });



    return LeafletLayer;
});