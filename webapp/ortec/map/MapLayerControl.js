sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/Control',
], function(jQuery, Control) {
    'use strict';

    var MapLayerControl = Control.extend('ortec.map.MapLayerControl', {
        metadata: {
            library: 'ortec.map',
            aggregations: {
                layers: { type: 'ortec.map.LeafletLayer', multiple: true, singularName: 'layer'},
            },
            associations: {
                containerLeafletControl: { type: 'ortec.map.LeafletControl' },
            },
            events: {
                afterLayerRendering: {},
            },
            defaultAggregation: 'layers'
        },

        constructor: function () {
            // call base-constructor
            Control.apply(this, arguments);
        },

        init: function () {
            Control.prototype.init && Control.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            jQuery.sap.log.info('MapLayerControl initialized');
        },

        destroy: function () {
            Control.prototype.destroy && Control.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('MapLayerControl destroyed');
        },

        exit: function () {
            Control.prototype.exit && Control.prototype.exit.apply(this, arguments);

            jQuery.sap.log.info('MapLayerControl exited');
        },

        renderer: function (oRm, oControl) {
            oRm.write('<div');
            oRm.writeControlData(oControl);
            oRm.write('></div>');
        },

        rerender: function () {
            var sMapObjectId = this.getContainerLeafletControl();
            var oUi5MapControl = sap.ui.getCore().byId(sMapObjectId);
            var oMapObject = oUi5MapControl && oUi5MapControl.getMapObject();

            oMapObject && this._attachLayers(oMapObject);
        },

        onAfterRendering: function () {
            this.rerender();
        },

        _attachLayers: function (oMapObject) {
            var aLayers = this.getLayers();

            for (var i = 0; i < aLayers.length; i++) {
                var oLayer = aLayers[i];

                oLayer.attachToMapObject(oMapObject);
                jQuery.sap.log.info('Layer added to MapLayerControl, key: ' + oLayer.getKey());
            }

            this.fireAfterLayerRendering();
        },

    });



    return MapLayerControl;
});