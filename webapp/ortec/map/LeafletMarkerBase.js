sap.ui.define([
    'jquery.sap.global',
    './LeafletLayer',
    './LeafletWaypoint'
], function(jQuery, LeafletLayer) {
    'use strict';

    var LPopupInitLayout = L.Popup.prototype._initLayout;

    L.Popup.prototype._initLayout = function () {
        var result = LPopupInitLayout.apply(this, arguments);

        if (this.options && this.options.enableEventBubbling) {
            L.DomEvent.off(this._wrapper, 'mousedown touchstart dblclick', L.DomEvent.stopPropagation);
        }

        return result;
    };

    var LeafletMarkerBase = LeafletLayer.extend('ortec.map.LeafletMarkerBase', {
        metadata: {
            library: 'ortec.map',
            abstract: true,
            properties: {
                selected: { type: 'boolean', defaultValue: false },
                draggable: { type: 'boolean', defaultValue: false },
                interactive: { type: 'boolean', defaultValue: false },
                keyboard: { type: 'boolean', defaultValue: true },

                opacity: { type: 'float', defaultValue: 1.0 },
                riseOnHover: { type: 'boolean', defaultValue: false },
                riseOffset: { type: 'int', defaultValue: 250 },

                position: { type: 'float[]', defaultValue: [] },
                positionLatitude: { type: 'float', defaultValue: 0 },
                positionLongitude: { type: 'float', defaultValue: 0 },

                additionalEventParameters: { type: 'object' },
            },
            aggregations: {
                popup: { type: 'sap.ui.core.Control', singularName: 'popup', multiple: true },
            },
            events: {
                press: {},
                dragEnd: {},
                dragStart: {},
                drag: {},
                mouseOver: {},
                mouseOut: {}
            },
            defaultAggregation: 'popup',
        },

        _bIsDragging: false,

        _onMarkerPressDelegate: undefined,
        _onMarkerDragStartDelegate: undefined,
        _onMarkerDragEndDelegate: undefined,
        _onMarkerDragDelegate: undefined,
        _onMarkerMouseOverDelegate: undefined,
        _onMarkerMouseOutDelegate: undefined,

        _oPopupDomElement: undefined,
        _oRenderManager: undefined,

        init: function() {
            LeafletLayer.prototype.init && LeafletLayer.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            this._oPopupDomElement = document.createElement('div');
            this._oRenderManager = sap.ui.getCore().createRenderManager();

            this._onMarkerPressDelegate = this._onMarkerPress.bind(this);
            this._onMarkerDragStartDelegate = this._onMarkerDragStart.bind(this);
            this._onMarkerDragEndDelegate = this._onMarkerDragEnd.bind(this);
            this._onMarkerDragDelegate = this._onMarkerDrag.bind(this);
            this._onMarkerMouseOverDelegate = this._onMarkerMouseOver.bind(this);
            this._onMarkerMouseOutDelegate = this._onMarkerMouseOut.bind(this);

            jQuery.sap.log.info('LeafletMarkerBase initialized');
        },

        destroy: function() {
            LeafletLayer.prototype.destroy && LeafletLayer.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletMarkerBase destroyed');
        },

        exit: function() {
            LeafletLayer.prototype.exit && LeafletLayer.prototype.exit.apply(this, arguments);

            this.detach();

            this._oRenderManager.destroy();
            this._oRenderManager = null;
            this._oPopupDomElement = null;

            jQuery.sap.log.info('LeafletMarkerBase exited');
        },

        setPosition: function (aPosition) {
            if (aPosition && aPosition.length) {
                var iLatitude = (aPosition.length >= 1 && aPosition[0]) || undefined;
                var iLongitude = (aPosition.length >= 2 && aPosition[1]) || undefined;

                this._setPosition(iLatitude, iLongitude);
            } else {
                jQuery.sap.log.warning('LeafletMarkerBase.setPosition requires an array [lat, lon] as input');
            }
        },

        setPositionLatitude: function (iLatitude) {
            this._setPosition(iLatitude);
        },

        setPositionLongitude: function (iLongitude) {
            this._setPosition(undefined, iLongitude);
        },

        _setPosition: function (iPositionLatitude, iPositionLongitude) {
            var oLayer = this.getLayer();

            var sTypeOfPositionLatitude = typeof iPositionLatitude;
            var sTypeOfPositionLongitude = typeof iPositionLongitude;

            var aPosition = this.getProperty('position');
            var iPositionLatitudeFromArray = (aPosition.length >= 1 && aPosition[0]) || undefined;
            var iPositionLongitudeFromArray = (aPosition.length >= 2 && aPosition[1]) || undefined;

            var iPositionLatitudeFromProperty = this.getPositionLatitude();
            var iPositionLongitudeFromProperty = this.getPositionLongitude();


            if (sTypeOfPositionLatitude === 'string') {
                iPositionLatitude = parseFloat(iPositionLatitude);
            } else if (sTypeOfPositionLatitude !== 'number') {
                iPositionLatitude = iPositionLatitudeFromProperty || iPositionLatitudeFromArray || 0;
            }

            if (sTypeOfPositionLongitude === 'string') {
                iPositionLongitude = parseFloat(iPositionLongitude);
            } else if (sTypeOfPositionLongitude !== 'number') {
                iPositionLongitude = iPositionLongitudeFromProperty || iPositionLongitudeFromArray || 0;
            }


            this.setProperty('position', [iPositionLatitude, iPositionLongitude]);
            this.setProperty('positionLatitude', iPositionLatitude, true);
            this.setProperty('positionLongitude', iPositionLongitude, true);


            if (oLayer) {
                oLayer.setLatLng({
                    lat: iPositionLatitude,
                    lng: iPositionLongitude,
                });
            }
        },

        getPosition: function () {
            var aPosition = this.getProperty('position');
            var iPositionLatitudeFromArray = (aPosition.length >= 1 && aPosition[0]) || undefined;
            var iPositionLongitudeFromArray = (aPosition.length >= 2 && aPosition[1]) || undefined;

            var iPositionLatitudeFromProperty = this.getPositionLatitude();
            var iPositionLongitudeFromProperty = this.getPositionLongitude();

            var iPositionLatitude = iPositionLatitudeFromProperty || iPositionLatitudeFromArray || 0;
            var iPositionLongitude = iPositionLongitudeFromProperty || iPositionLongitudeFromArray || 0;

            return [iPositionLatitude, iPositionLongitude];
        },

        getIcon: function() {
            return new L.Icon.Default();
        },

        openPopup: function () {
            var oLayer = this.getLayer();

            var oPopup = oLayer && oLayer.getPopup();
            var bIsPopupOpen = oPopup && oLayer.isPopupOpen();

            if (!!oPopup && !bIsPopupOpen) {
                oLayer.openPopup();
            }
        },

        closePopup: function () {
            var oLayer = this.getLayer();

            var oPopup = oLayer && oLayer.getPopup();
            var bIsPopupOpen = oPopup && oLayer.isPopupOpen();

            if (!!oPopup && bIsPopupOpen) {
                oLayer.closePopup();
            }
        },

        rerenderLayerObject: function() {
            // this.detach();

            var oIcon = this.getIcon();

            // put selected Markers always on top
            var iZIndex = this.getSelected() ? 1000 : 0;

            try {
                var oLayer = this.getLayer();

                if (!oLayer) {
                    oLayer = L.marker(this.getPosition(), {
                        icon:  oIcon,
                        zIndexOffset: iZIndex,
                        draggable: this.getDraggable(),
                        interactive: this.getInteractive(),
                        keyboard: this.getKeyboard(),
                        opacity: this.getOpacity(),
                        riseOnHover: this.getRiseOnHover(),
                        riseOffset: this.getRiseOffset(),
                    });

                    // bind events
                    oLayer.on('click', this._onMarkerPressDelegate);
                    oLayer.on('drag', this._onMarkerDragDelegate);
                    oLayer.on('dragstart', this._onMarkerDragStartDelegate);
                    oLayer.on('dragend', this._onMarkerDragEndDelegate);
                    oLayer.on('mouseover', this._onMarkerMouseOverDelegate);
                    oLayer.on('mouseout', this._onMarkerMouseOutDelegate);
                } else if (!this._bIsDragging) {
                    var iPositionLatitude = this.getPositionLatitude();
                    var iPositionLongitude = this.getPositionLongitude();

                    oLayer.setLatLng({
                        lat: iPositionLatitude,
                        lng: iPositionLongitude,
                    });

                    oLayer.setIcon(oIcon);
                    oLayer.setOpacity(this.getOpacity());
                    oLayer.setZIndexOffset(iZIndex);

                    oLayer.update();
                }

                this._oPopupDomElement.innerHTML = '';
                var aPopupContent = this.getPopup();

                if (aPopupContent.length === 1) {
                    var oControl = aPopupContent[0];

                    this._oRenderManager.renderControl(oControl);
                    this._oRenderManager.flush(this._oPopupDomElement);
                } else if (aPopupContent.length > 1) {
                    for (var i = 0; i < aPopupContent.length; i++) {
                        var oControl = aPopupContent[i];

                        this._oRenderManager.renderControl(oControl);
                    }

                    this._oRenderManager.flush(this._oPopupDomElement);
                }

                if (this._oPopupDomElement.innerHTML && this._oPopupDomElement.innerHTML !== '') {
                    var oCurrentBoundPopup = oLayer.getPopup();
                    var oCurrentContent = oCurrentBoundPopup && oCurrentBoundPopup.getContent();

                    if (oCurrentContent !== this._oPopupDomElement) {
                        oLayer.bindPopup(this._oPopupDomElement, {
                            enableEventBubbling: true,
                        });
                    }
                }
            } catch (err) {
                jQuery.sap.log.warning('LeafletMarkerBase.rerenderLayerObject failed');
                oLayer = undefined
            }

            this.setProperty('layer', oLayer, true);

            return oLayer;
        },

        _combineEventParametersWithAdditionalParameters: function (oEvent) {
            var oAdditionalParameters = this.getAdditionalEventParameters();

            if (oAdditionalParameters) {
                return jQuery.extend({}, oEvent, oAdditionalParameters);
            } else {
                return oEvent;
            }
        },

        _onMarkerPress: function (oEvent) {
            this.firePress(this._combineEventParametersWithAdditionalParameters(oEvent));
        },
        _onMarkerDragStart: function (oEvent) {
            this._bIsDragging = true;

            this.fireDragStart(this._combineEventParametersWithAdditionalParameters(oEvent));
        },
        _onMarkerDragEnd: function (oEvent) {
            var oLatLng = oEvent.target && oEvent.target.getLatLng();

            this._setPosition(oLatLng.lat, oLatLng.lng);

            this._bIsDragging = false;

            this.fireDragEnd(this._combineEventParametersWithAdditionalParameters(oEvent));
        },
        _onMarkerDrag: function (oEvent) {
            this.fireDrag(this._combineEventParametersWithAdditionalParameters(oEvent));
        },
        _onMarkerMouseOver: function (oEvent) {
            this.fireMouseOver(this._combineEventParametersWithAdditionalParameters(oEvent));
        },
        _onMarkerMouseOut: function (oEvent) {
            this.fireMouseOut(this._combineEventParametersWithAdditionalParameters(oEvent));
        },
    });



    return LeafletMarkerBase;
});