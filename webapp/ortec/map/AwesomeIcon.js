sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/IconPool',
    './LeafletMarkerBase',
    './third-party/awesome-markers/leaflet.awesome-markers',
], function(jQuery, IconPool, LeafletMarkerBase) {
    'use strict';

    L.AwesomeMarkers.Icon.createInternalIcon = L.AwesomeMarkers.Icon.prototype.createInternalIcon = L.AwesomeMarkers.Icon.prototype.createIcon;

    L.AwesomeMarkers.Icon.createIcon = L.AwesomeMarkers.Icon.prototype.createIcon = function () {
        var options = this.options;

        if (!options.useSapIcons) {
            L.AwesomeMarkers.Icon.prototype.createInternalIcon.apply(this, arguments);
        } else if (!!options.icon) {
            // create icon from SAP

            var div = document.createElement('div');
            var oIconInfo = IconPool.getIconInfo(options.icon);

            if (oIconInfo) {
                var sIconSpinClass = '';
                var sStyle = 'cursor: pointer; position:relative; top:0; font-family:' + oIconInfo.fontFamily + ';';

                if (options.iconColor) sStyle += 'color:' + options.iconColor + ';';
                if (options.fontSize) sStyle += 'font-size:' + options.fontSize+';';

                if(options.spin && typeof options.spinClass === "string") {
                    sIconSpinClass = options.spinClass;
                }

                div.innerHTML = '<span  style="' + sStyle + '" class="sapMBtnCustomIcon sapMBtnIcon sapMBtnIconLeft sapUiIcon sapUiIconMirrorInRTL ' + sIconSpinClass + '" data-sap-ui-icon-content="' + oIconInfo.content + '"> </span>';
            }  else {
                jQuery.sap.log.warning("Invalid SAP Icon URL in parameter options.icon");
            }

            if (options.bgPos) {
                div.style.backgroundPosition = (-options.bgPos.x) + 'px ' + (-options.bgPos.y) + 'px';
            }

            this._setIconStyles(div, 'icon-' + options.markerColor);

            return div;
        }
    };


    var AwesomeIcon = LeafletMarkerBase.extend('ortec.map.AwesomeIcon', {
        metadata: {
            library: 'ortec.map',
            properties: {
                icon: { type: 'string', defaultValue: 'sap-icon://add' },
                prefix: { type: 'string', defaultValue: '' },

                markerColor: { type: 'string', defaultValue: 'red' },
                markerColorSelected: { type: 'string', defaultValue: 'green' },

                iconColor: { type: 'sap.ui.core.CSSColor', defaultValue: 'white' },
                iconColorSelected: { type: 'sap.ui.core.CSSColor', defaultValue: 'blue' },

                spin:  { type: 'boolean', defaultValue: false },
                spinClass:  { type: 'string', defaultValue: 'ortec-marker-spin' },
                useSapIcons: { type: 'boolean', defaultValue: true },
            },
        },

        constructor: function() {
            // call base-constructor
            LeafletMarkerBase.apply(this, arguments);

        },

        init: function() {
            LeafletMarkerBase.prototype.init && LeafletMarkerBase.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            jQuery.sap.log.info('AwesomeIcon initialized');
        },

        destroy: function() {
            LeafletMarkerBase.prototype.destroy && LeafletMarkerBase.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('AwesomeIcon destroyed');
        },

        exit: function() {
            LeafletMarkerBase.prototype.exit && LeafletMarkerBase.prototype.exit.apply(this, arguments);

            jQuery.sap.log.info('AwesomeIcon exited');
        },

        getIcon: function() {
            var oOptions = {
                icon: this.getProperty('icon'),
                useSapIcons: this.getUseSapIcons(),
                prefix: this.getPrefix(),
                spin: this.getSpin(),
                spinClass: this.getSpinClass(),
            };

            if (this.getSelected()) {
                oOptions.iconColor = this.getIconColorSelected();
                oOptions.markerColor = this.getMarkerColorSelected();
            } else {
                oOptions.iconColor = this.getIconColor();
                oOptions.markerColor = this.getMarkerColor();
            }

            return new L.AwesomeMarkers.icon(oOptions);
        },

    });



    return AwesomeIcon;
});