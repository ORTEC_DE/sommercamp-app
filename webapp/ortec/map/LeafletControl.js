sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/Control',
    'sap/ui/core/ResizeHandler',
    './library',
    './LeafletLayer',
    './LeafletTileLayer',
    './LeafletLayerGroup',
], function(jQuery, Control, ResizeHandler, OrtecMapLibrary, LeafletLayer, LeafletTileLayer, LeafletLayerGroup) {
    'use strict';

    var LeafletControl = Control.extend('ortec.map.LeafletControl', {
        metadata: {
            library: 'ortec.map',
            properties: {
                name: { type: 'string', defaultValue: null },

                width: { type: 'sap.ui.core.CSSSize', defaultValue: '100%' },
                height: { type: 'sap.ui.core.CSSSize', defaultValue: '100%' },

                zoomControl: { type: 'boolean', defaultValue: true },
                attributionControl: { type: 'boolean', defaultValue: true },

                keyboard: { type: 'boolean', defaultValue: true },
                scrollWheelZoom: { type: 'boolean', defaultValue: true },

                zoom: { type: 'int', defaultValue: 13 },
                minZoom: { type: 'int', defaultValue: 0 },
                maxZoom: { type: 'int', defaultValue: 18 },

                center: { type: 'float[]', defaultValue: []},
                centerLatitude: { type: 'float', defaultValue: 0 },
                centerLongitude: { type: 'float', defaultValue: 0 },
                autoCenter: { type: 'boolean', defaultValue: true },

                selectedTileLayer: { type: 'string', defaultValue: null },
            },
            aggregations: {
                tileLayers: { type: 'ortec.map.LeafletTileLayer', multiple: true, singularName: 'tileLayer', bindable: 'bindable' },
                layers: { type: 'ortec.map.LeafletLayer', multiple: true, singularName: 'layer', bindable: 'bindable' },
            },
            events: {
                afterLayerRendering: {},

                moveEnd: {},
                zoomEnd: {},
            },
            defaultAggregation: 'layers'
        },

        _bInitialized: false,
        _sResizeHandlerRegistrationId: undefined,

        _bIsZooming: false,
        _bIsMoving: false,

        _oMapContainer: undefined,
        _oMapObject: undefined,
        _oLayersControl: undefined,

        constructor: function () {
            // call base-constructor
            Control.apply(this, arguments);
        },

        init: function () {
            Control.prototype.init && Control.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            // register for resize event
            this._sResizeHandlerRegistrationId = ResizeHandler.register(this, this._onResize.bind(this));

            // build DOM-Container for control & set style
            this._oMapContainer = document.createElement('div');
            this._oMapContainer.style.width = '100%';
            this._oMapContainer.style.height = '100%';

            jQuery.sap.log.info('LeafletControl initialized');
        },

        destroy: function () {
            Control.prototype.destroy && Control.prototype.destroy.apply(this, arguments);

            // deregister event for this control
            this._sResizeHandlerRegistrationId && ResizeHandler.deregister(this._sResizeHandlerRegistrationId);

            // destroy the DOM-Container
            this._oMapContainer.remove();
            this._oMapContainer = null;

            jQuery.sap.log.info('LeafletControl destroyed');
        },

        exit: function () {
            Control.prototype.exit && Control.prototype.exit.apply(this, arguments);

            jQuery.sap.log.info('LeafletControl exited');
        },

        getMapObject: function () {
            return this._oMapObject;
        },

        setMapObject: function (oMapObject) {
            this._oMapObject = oMapObject;
        },

        setCenter: function (aCenter) {
            if (aCenter && aCenter.length) {
                var iLatitude = (aCenter.length >= 1 && aCenter[0]) || undefined;
                var iLongitude = (aCenter.length >= 2 && aCenter[1]) || undefined;

                this._setCenter(iLatitude, iLongitude);
            } else {
                jQuery.sap.log.warning('LeafletControl.setCenter requires an array [lat, lon] as input');
            }
        },

        setCenterLatitude: function (iCenterLatitude) {
            this._setCenter(iCenterLatitude);
        },

        setCenterLongitude: function (iCenterLongitude) {
            this._setCenter(undefined, iCenterLongitude);
        },

        setCenterFromLatLng: function(oLatLng, bReCenter) {
            this._setCenter(oLatLng.lat, oLatLng.lng);

            bReCenter && this.reCenter();
            return this;
        },

        _setCenter: function (iCenterLatitude, iCenterLongitude, suppressInvalidate) {
            var bSuppressInvalidate = !!suppressInvalidate;

            var aCenter = this.getProperty('center');
            var iCenterLatitudeFromArray = (aCenter.length >= 1 && aCenter[0]) || undefined;
            var iCenterLongitudeFromArray = (aCenter.length >= 2 && aCenter[1]) || undefined;

            var iCenterLatitudeFromProperty = this.getCenterLatitude();
            var iCenterLongitudeFromProperty = this.getCenterLongitude();

            iCenterLatitude = iCenterLatitude || iCenterLatitudeFromProperty || iCenterLatitudeFromArray || 0;
            iCenterLongitude = iCenterLongitude || iCenterLongitudeFromProperty || iCenterLongitudeFromArray || 0;

            if (typeof iCenterLatitude === 'string') {
                iCenterLatitude = parseFloat(iCenterLatitude);
            }

            if (typeof iCenterLongitude === 'string') {
                iCenterLongitude = parseFloat(iCenterLongitude);
            }

            this.setProperty('centerLatitude', iCenterLatitude, true);
            this.setProperty('centerLongitude', iCenterLongitude, true);
            this.setProperty('center', [iCenterLatitude, iCenterLongitude], bSuppressInvalidate);
        },

        getCenter: function () {
            var aCenter = this.getProperty('center');
            var iCenterLatitudeFromArray = (aCenter.length >= 1 && aCenter[0]) || undefined;
            var iCenterLongitudeFromArray = (aCenter.length >= 2 && aCenter[1]) || undefined;

            var iCenterLatitudeFromProperty = this.getCenterLatitude();
            var iCenterLongitudeFromProperty = this.getCenterLongitude();

            var iCenterLatitude =  iCenterLatitudeFromProperty || iCenterLatitudeFromArray || 0;
            var iCenterLongitude = iCenterLongitudeFromProperty || iCenterLongitudeFromArray || 0;

            return [iCenterLatitude, iCenterLongitude];
        },

        renderer: function (oRm, oControl) {
            oRm.write('<div');
            oRm.writeControlData(oControl);
            oRm.addStyle('width', oControl.getWidth());
            oRm.addStyle('height', oControl.getHeight());
            oRm.writeStyles();
            oRm.write('></div>');
        },

        rerender: function () {
            this.onAfterRendering();

            var oMapObject = this.getMapObject();

            if (oMapObject) {
                oMapObject.setMinZoom(this.getMinZoom());
                oMapObject.setMaxZoom(this.getMaxZoom());

                var aCenter = this.getCenter();

                if (!this._bIsZooming && !this._bIsMoving) {
                    oMapObject.setView(aCenter, this.getZoom());
                }

                this._attachLayers(oMapObject);
            }
        },

        onAfterRendering: function () {
            if (this._oMapContainer) {
                var oDomRef = this.getDomRef();

                if (!oDomRef.contains(this._oMapContainer)) {
                    oDomRef.appendChild(this._oMapContainer);
                }
            }

            if (!this._bInitialized) {
                var oMapObject = this._initMapObject();
                oMapObject.invalidateSize(true);

                this._bInitialized = !!oMapObject;
            }
        },

        _onResize: function (oEvent) {
            var oMapObject = this.getMapObject();

            oMapObject && oMapObject.invalidateSize(true);
        },

        _onMapZoomStart: function (oEvent) {
            this._bIsZooming = true;
        },
        _onMapZoomEnd: function (oEvent) {
            this._bIsZooming = false;

            var iZoom = oEvent.target.getZoom();

            this.setProperty('zoom', iZoom, true);

            this.fireZoomEnd({
                zoom: iZoom
            });
        },

        _onMapMoveStart: function (oEvent) {
            this._bIsMoving = true;
        },
        _onMapMoveEnd: function (oEvent) {
            this._bIsMoving = false;

            var aCenter = oEvent.target.getCenter();

            var iLatitude = aCenter.lat;
            var iLongitude = aCenter.lng;

            this._setCenter(iLatitude, iLongitude, true);

            this.fireMoveEnd({
                latitude: iLatitude,
                longitude: iLongitude,
            });
        },

        _initMapObject: function () {
            var oMetadata = this.getMetadata();
            var oProperties = oMetadata.getProperties();
            var oOptions = {};

            for (var oProperty in oProperties) {
                if (oProperties.hasOwnProperty(oProperty)) {
                    switch (oProperty) {
                        case 'name':
                        case 'width':
                        case 'height':
                        case 'autoCenter':
                        case 'centerLatitude':
                        case 'centerLongitude':
                        case 'selectedTileLayer': {
                            // don't import control specific properties into map configuration object
                            continue;
                        }

                        case 'center': {
                            oOptions[oProperty] = this.getCenter();
                            break;
                        }

                        default: {
                            oOptions[oProperty] = this.getProperty(oProperty);
                        }
                    }
                }
            }

            var oMapObject = L.map(this._oMapContainer, oOptions);


            oMapObject.on('zoomstart', this._onMapZoomStart, this);
            oMapObject.on('zoomend', this._onMapZoomEnd, this);

            oMapObject.on('movestart', this._onMapMoveStart, this);
            oMapObject.on('moveend', this._onMapMoveEnd, this);

            this.setMapObject(oMapObject);

            this._initTileLayers(oMapObject);
            this._attachLayers(oMapObject);

            return oMapObject;
        },

        _initTileLayers: function (oMapObject) {
            var oLayerConfig = {};
            var aTileLayers = this.getTileLayers();
            var sSelectedTileLayer = this.getSelectedTileLayer();

            for (var i = 0; i < aTileLayers.length; i++) {
                var oTileLayer = aTileLayers[i];

                var sTileLayerName = oTileLayer.getName() || 'Layer ' + (i + 1);
                var sTileLayerSource = oTileLayer.getTileSource();
                var oTileLayerOptions = oTileLayer.getOptionsObject();

                oLayerConfig[sTileLayerName] = L.tileLayer(sTileLayerSource, oTileLayerOptions);
            }

            // add a LayersControl when we have more Layers than 1
            if (aTileLayers.length > 1) {
                var oLayersControl = L.control.layers(oLayerConfig);
                oLayersControl.addTo(oMapObject);
                this._oLayersControl = oLayersControl;
            }

            if (sSelectedTileLayer && sSelectedTileLayer in oLayerConfig) {
                // load selectes layer into map
                oLayerConfig[sSelectedTileLayer].addTo(oMapObject);
                jQuery.sap.log.info('Use selected TileLayer');
            } else if (aTileLayers.length > 0) {
                // load first layer into map
                var sFirstLayerName = aTileLayers[0].getName() || 'Layer 1';

                oLayerConfig[sFirstLayerName] && oLayerConfig[sFirstLayerName].addTo(oMapObject);
                jQuery.sap.log.info('Use first existing TileLayer, because we can not fing any TileLayer with name: ' + sTileLayerName);
            } else {
                jQuery.sap.log.warn('TileLayer Aggregation seems to be empty. Please provide some TileLayers');
            }

        },

        _attachLayers: function (oMapObject) {
            var aLayers = this.getLayers();

            for (var i = 0; i < aLayers.length; i++) {
                var oLayer = aLayers[i];

                oLayer.attachToMapObject(oMapObject);
                jQuery.sap.log.info('Layer added to Leaflet map, key: ' + oLayer.getKey());
            }

            this.fireAfterLayerRendering();
        },

        /*
         * Update the map view according to the current value of the center and zoom properties.
         * Not needed when the autoCenter property is set to true - in this case changing the zoom and center properties
         * automatically update the map
         */
        reCenter: function() {
            var oMapObject = this.getMapObject();

            oMapObject && oMapObject.setView(this.getCenter(), this.getZoom());
            return this;
        },

        /*
         * just wrapper around MapObj method
         */
        panTo: function(oLatLng, oPanOptions) {
            var oMapObject = this.getMapObject();

            oMapObject && oMapObject.panTo(oLatLng, oPanOptions);
            return this;
        },

        /*
         * Like panTo - but only pans when the given coordinates are not currently in the view of the map
         */
        panToIfNeeded: function(oLatLng, oPanOptions) {
            var oMapObject = this.getMapObject();
            var oBounds = this.getBounds();

            if (oBounds && !oBounds.contains(L.latLngBounds(oLatLng, oLatLng))) {
                this.panTo(oLatLng, oPanOptions);
            }

            return this;
        },

        /*
         * just wrapper around MapObj method
         */
        getBounds: function() {
            var oMapObject = this.getMapObject();

            return (oMapObject && oMapObject.getBounds()) || null;
        },

        fitBounds: function(oBounds, oFitBoundsOptions) {
            var oMapObject = this.getMapObject();

            oMapObject && oMapObject.fitBounds(oBounds, oFitBoundsOptions);

            return this;
        },

    });



    return LeafletControl;
});