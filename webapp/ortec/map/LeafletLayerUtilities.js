sap.ui.define([
    'jquery.sap.global',
], function(jQuery) {
    'use strict';

    var LeafletLayerUtilities = {

        /*
         * Calculates the Bonding Box of an Array of objects. The type of object doesn't matter. The lat/lon is extracted either by name (when a string with the
         * property name is passed in getLat and/or getLon, or with a callback function (in case a function is passed in getLat/getLon respectily
         * e.g when the Array contains objects with a Property  named "Latitude", the String "Latitude" should be passed in parameter getLat.
         */
        calculateBoundingBox: function(aInput, getLat, getLon)
        {

            var west = 0.0;
            var north = 0.0;
            var south = 0.0;
            var east = 0.0;
            var first = true; // flag for first iteration

            for (var i = 0; i <  aInput.length; i++) {
                var oPoint = aInput[i];

                var x;
                var y;

                if (typeof getLat === 'function') {
                    y = getLat(oPoint);
                } else {
                    y = oPoint[getLat];
                }

                if (typeof getLon === 'function') {
                    x = getLon(oPoint);
                } else {
                    x = oPoint[getLon];
                }

                if (x && !isNaN(x) && y && !isNaN(y)) { // skip elements wich have invalid coordinates
                    if (first) {
                        west = x;
                        east = x;
                        north = y;
                        south = y;
                        first=false;
                    } else {
                        if (x < west) west=x;
                        if (x > east) east=x;
                        if (y > north) north=y; // Latitude (y) grows to North : North pole +90, south pole -90
                        if (y < south) south=y;

                    }
                }
            }

            return  L.latLngBounds(L.latLng(south, west), L.latLng(north, east));
        },

    };

    return LeafletLayerUtilities;
});