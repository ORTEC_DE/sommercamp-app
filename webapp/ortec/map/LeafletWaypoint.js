sap.ui.define([
    'jquery.sap.global',
    'sap/ui/core/Element',
], function(jQuery, Element) {
    'use strict';

    var LeafletWaypoint = Element.extend('ortec.map.LeafletWaypoint', {
        metadata: {
            library: 'ortec.map',
            abstract: false,
            properties: {
                position: { type: 'float[]', defaultValue: [] },
                positionLatitude: { type: 'float', defaultValue: 0 },
                positionLongitude: { type: 'float', defaultValue: 0 },
            },
            aggregations: {},
            events: {},
        },

        init: function() {
            Element.prototype.init && Element.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            jQuery.sap.log.info('LeafletWaypoint initialized');
        },

        destroy: function() {
            Element.prototype.destroy && Element.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletWaypoint destroyed');
        },

        exit: function() {
            Element.prototype.exit && Element.prototype.exit.apply(this, arguments);

            jQuery.sap.log.info('LeafletWaypoint exited');
        },

        setPosition: function (aPosition) {
            if (aPosition && aPosition.length) {
                var iLatitude = (aPosition.length >= 1 && aPosition[0]) || undefined;
                var iLongitude = (aPosition.length >= 2 && aPosition[1]) || undefined;

                this._setPosition(iLatitude, iLongitude);
            } else {
                jQuery.sap.log.warning('LeafletWaypoint.setPosition requires an array [lat, lon] as input');
            }
        },

        setPositionLatitude: function (iLatitude) {
            this._setPosition(iLatitude);
        },

        setPositionLongitude: function (iLongitude) {
            this._setPosition(undefined, iLongitude);
        },

        _setPosition: function (iPositionLatitude, iPositionLongitude) {
            var aPosition = this.getPosition();
            var iPositionLatitudeFromArray = (aPosition.length >= 1 && aPosition[0]) || undefined;
            var iPositionLongitudeFromArray = (aPosition.length >= 2 && aPosition[1]) || undefined;

            var iPositionLatitudeFromProperty = this.getPositionLatitude();
            var iPositionLongitudeFromProperty = this.getPositionLongitude();

            iPositionLatitude = iPositionLatitude || iPositionLatitudeFromProperty || iPositionLatitudeFromArray || 0;
            iPositionLongitude = iPositionLongitude || iPositionLongitudeFromProperty || iPositionLongitudeFromArray || 0;

            if (typeof iPositionLatitude === 'string') {
                iPositionLatitude = parseFloat(iPositionLatitude);
            }

            if (typeof iPositionLongitude === 'string') {
                iPositionLongitude = parseFloat(iPositionLongitude);
            }

            this.setProperty('positionLatitude', iPositionLatitude, true);
            this.setProperty('positionLongitude', iPositionLongitude, true);
            this.setProperty('position', [iPositionLatitude, iPositionLongitude]);
        },

    });



    return LeafletWaypoint;
});