sap.ui.define([
    'jquery.sap.global',
    './LeafletLayer',
    './LeafletLayerGroup',
    './LeafletTripSegment',
    './LeafletMarkerBase',
    './LeafletWaypoint',
    './LeafletLayerUtilities'
], function(jQuery, LeafletLayer, LeafletLayerGroup, LeafletTripSegment, LeafletMarkerBase, LeafletWaypoint, LeafletLayerUtilities) {
    'use strict';

    var LeafletTrip = LeafletLayerGroup.extend('ortec.map.LeafletTrip', {
        metadata: {
            library: 'ortec.map',
            properties: {
                allowSegmentSelection: { type: 'boolean', defaultValue: false },
                color: { type: 'sap.ui.core.CSSColor', defaultValue: '#000' },
                colorSelected: { type: 'sap.ui.core.CSSColor', defaultValue: '#000' },
            },
            aggregations: {
                stops: { type: 'ortec.map.LeafletMarkerBase', singularName: 'stop', multiple: true },
            },
            events: {
                pressStop: {},
                pressSegment: {},
            },
            defaultAggregation: 'stops',
        },

        _bRebuildNeeded: false,

        _onStopPressDelegate: undefined,
        _onSegmentPressDelegate: undefined,

        _segmentFactoryDelegate: undefined,

        init: function() {
            LeafletLayerGroup.prototype.init && LeafletLayerGroup.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            this._onStopPressDelegate = this._onStopPress.bind(this);
            this._onSegmentPressDelegate = this._onSegmentPress.bind(this);

            jQuery.sap.log.info('LeafletTrip initialized');
        },

        destroy: function() {
            LeafletLayerGroup.prototype.destroy && LeafletLayerGroup.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletTrip destroyed');
        },

        exit: function() {
            LeafletLayerGroup.prototype.exit && LeafletLayerGroup.prototype.exit.apply(this, arguments);

            jQuery.sap.log.info('LeafletTrip exited');
        },

        invalidate: function (oElement) {
            if (oElement instanceof LeafletMarkerBase) {
                this._bRebuildNeeded = true;
            }

            return LeafletLayer.prototype.invalidate.apply(this, arguments);
        },

        buildSegments: function () {
            var sTripColor = this.getColor();
            var sTripColorSelected = this.getColorSelected();
            var aStops = this.getStops();

            // reset child layers
            this.destroyChildren();

            for (var i = 0; i < aStops.length; i++) {
                var oStopMarker = aStops[i];

                if (i < aStops.length - 1) {
                    var oStartPoint = this._getWaypointFromStop(oStopMarker);
                    var oEndPoint = this._getWaypointFromStop(aStops[i + 1]);

                    if (oStartPoint && oEndPoint) {
                        var oSegment = new LeafletTripSegment({
                            color: sTripColor,
                            colorSelected: sTripColorSelected,

                            press: this._onSegmentPressDelegate,

                            waypoints: [
                                oStartPoint,
                                oEndPoint
                            ]
                        });

                        this.addChild(oSegment);
                    }
                }

                this.addChild(oStopMarker.clone());
            }
        },

        rerenderLayerObject: function() {
            if (this._bRebuildNeeded) {
                this.buildSegments();

                this._bRebuildNeeded = false;
            }

            return LeafletLayerGroup.prototype.rerenderLayerObject.apply(this, arguments);
        },

        calculateBoundingBox: function () {
            var aStops = this.getStops();

            return LeafletLayerUtilities.calculateBoundingBox(
                aStops,
                function (oStop) {
                    return oStop.getPositionLatitude();
                },
                function (oStop) {
                    return oStop.getPositionLongitude();
                }
            );
        },

        _getWaypointFromStop: function (oStop) {
            var aPosition = oStop && oStop.getPosition();
            var iLatitude = oStop && oStop.getPositionLatitude();
            var iLongitude = oStop && oStop.getPositionLongitude();

            if (aPosition) {
                return new LeafletWaypoint({
                    position: aPosition,
                    positionLatitude: iLatitude,
                    positionLongitude: iLongitude,
                });
            } else {
                return null;
            }
        },

        _onStopPress: function (oEvent) {
            var oStop = oEvent.getSource();
            var bSelected = oStop.getSelected();

            oStop.setSelected(!bSelected);
            this.firePressStop(oEvent);
        },
        _onSegmentPress: function (oEvent) {
            var oSegment = oEvent.getSource();
            var bSelected = oSegment.getSelected();
            var bAllowSegmentSelection = this.getAllowSegmentSelection();

            if (bAllowSegmentSelection) {
                oSegment.setSelected(!bSelected);
                this.firePressSegment(oEvent);
            }
        },
    });



    return LeafletTrip;
});