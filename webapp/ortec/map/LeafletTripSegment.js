sap.ui.define([
    'jquery.sap.global',
    './LeafletLayer',
    './LeafletMarkerBase',
], function(jQuery, LeafletLayer, LeafletMarkerBase) {
    'use strict';

    var LeafletTripSegment = LeafletLayer.extend('ortec.map.LeafletTripSegment', {
        metadata: {
            library: 'ortec.map',
            properties: {
                selected: { type: 'boolean', defaultValue: false },
                showOnlyEndpoints: { type: 'boolean', defaultValue: true },

                color: { type: 'sap.ui.core.CSSColor', defaultValue: '#000' },
                colorSelected: { type: 'sap.ui.core.CSSColor', defaultValue: '#000' },

                weight: { type: 'int', defaultValue: 3 },
                weightSelected: { type: 'int', defaultValue: 6 },

                dash: { type: 'string', defaultValue: '' },
                dashSelected: { type: 'string', defaultValue: '' },
            },
            aggregations: {
                waypoints: { type: 'ortec.map.LeafletWaypoint', singularName: 'waypoint', multiple: true },
            },
            events: {
                press: {},
            },
            defaultAggregation: 'waypoints',
        },

        _onPressDelegate: undefined,

        init: function() {
            LeafletLayer.prototype.init && LeafletLayer.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            this._onPressDelegate = this._onPress.bind(this);

            jQuery.sap.log.info('LeafletTripSegment initialized');
        },

        destroy: function() {
            LeafletLayer.prototype.destroy && LeafletLayer.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletTripSegment destroyed');
        },

        exit: function() {
            LeafletLayer.prototype.exit && LeafletLayer.prototype.exit.apply(this, arguments);

            jQuery.sap.log.info('LeafletTripSegment exited');
        },

        rerenderLayerObject: function() {
            this.detach();

            try {
                var aWaypoints = this.getWaypoints();
                var aLatLng = [];

                if (aWaypoints.length < 2) {
                    throw new Error('LeafletTripSegment needs a least 2 waypoints');
                }

                if (this.getShowOnlyEndpoints()) {
                    var oStartPoint = aWaypoints[0];
                    var oEndPoint = aWaypoints[aWaypoints.length -1];

                    aLatLng[0] = oStartPoint.getPosition();
                    aLatLng[1] = oEndPoint.getPosition();
                } else {
                    for (var i = 0; i < aWaypoints.length; i++) {
                        var oWaypoint = aWaypoints[i];
                        var aWaypointLatLng = oWaypoint.getPosition();

                        aLatLng[i] = aWaypointLatLng;
                    }
                }


                var oOptions = {
                    opacity: 1,
                };

                if (this.getSelected()) {
                    oOptions.color = this.getColorSelected();
                    oOptions.weight = this.getWeightSelected();
                    oOptions.dashArray = this.getDashSelected();
                } else {
                    oOptions.color = this.getColor();
                    oOptions.weight = this.getWeight();
                    oOptions.dashArray = this.getDash();
                }

                var oLayer = L.polyline(aLatLng, oOptions);

                oLayer.on('click', this._onPressDelegate);

                this.setProperty('layer', oLayer, true);

                return oLayer;
            } catch (e) {
                jQuery.sap.log.error(e.message);

                this.setProperty('layer', undefined, true);
                return undefined;
            }
        },

        _onPress: function (oEvent) {
            this.firePress(oEvent);
        },
    });



    return LeafletTripSegment;
});