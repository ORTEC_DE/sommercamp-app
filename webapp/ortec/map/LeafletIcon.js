sap.ui.define([
    'jquery.sap.global',
    './LeafletMarkerBase',
], function(jQuery, LeafletMarkerBase) {
    'use strict';

    var LeafletIcon = LeafletMarkerBase.extend('ortec.map.LeafletIcon', {
        metadata: {
            library: 'ortec.map',
            properties: {
                iconClassNormal: { type: 'string', defaultValue: '' },
                iconClassSelected: { type: 'string', defaultValue: '' },
                iconSize:  { type: 'int', defaultValue: 12 },
            },
            aggregations: {
                content: { type: 'sap.ui.core.Control', singularName: 'content', multiple: true },
            },
            defaultAggregation: 'content',
        },

        _oContentDomElement: undefined,

        init: function() {
            LeafletMarkerBase.prototype.init && LeafletMarkerBase.prototype.init.apply(this, arguments); // call the method with the original arguments if available

            this._oContentDomElement = document.createElement('div');

            jQuery.sap.log.info('LeafletIcon initialized');
        },

        destroy: function() {
            LeafletMarkerBase.prototype.destroy && LeafletMarkerBase.prototype.destroy.apply(this, arguments);

            jQuery.sap.log.info('LeafletIcon destroyed');
        },

        exit: function() {
            LeafletMarkerBase.prototype.exit && LeafletMarkerBase.prototype.exit.apply(this, arguments);

            this._oContentDomElement = null;

            jQuery.sap.log.info('LeafletIcon exited');
        },

        getIcon: function() {
            this._oContentDomElement.innerHTML = '';
            var aContent = this.getContent();

            if (aContent.length === 1) {
                var oControl = aContent[0];

                this._oRenderManager.renderControl(oControl);
                this._oRenderManager.flush(this._oContentDomElement);

            } else if (aContent.length > 1) {
                for (var i = 0; i < aContent.length; i++) {
                    var oControl = aContent[i];

                    this._oRenderManager.renderControl(oControl);
                }

                this._oRenderManager.flush(this._oContentDomElement);
            }

            var options = {
                className: this.getSelected() ? this.getIconClassSelected() : this.getIconClassNormal(),
                html: this._oContentDomElement.innerHTML,
            };

            return new L.divIcon(options);
        },

    });



    return LeafletIcon;
});