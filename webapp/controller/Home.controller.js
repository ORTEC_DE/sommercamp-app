sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"../service/TaxiService",
	"../model/formatter"
], function(Controller, JSONModel, TaxiService, formatter) {
	"use strict";

	return Controller.extend("ortec.sommercamp.controller.Home", {

		// Verweis auf die Formatter-Sammlung im /webapp/modles Ordner
		formatter: formatter,

		// Referenz auf das Karten-Control
		map: undefined,

		/**
		 * Wird einmalig beim Erstellen des Controllers ausgeführt
		 */
		onInit: function () {
			// hole den View des Controllers
			var view = this.getView();

			// Abrufen der Karten-Control-Instanz aus dem Controller mittels ID
			this.map = view.byId('map');

			// Erstelle eine JSONModel-Instanz mit folgenden Daten
			var model = new JSONModel({
				// Die aktuelle Position des Users, welche durch 'updateCurrentGeoLocation' aktualisiert wird
				position: {
					lat: 49,
					lng: 8,
				},
				// Die aktuelle Position, sowie die Sichtbarkeit des Ziel-Markers
				destination: {
					markerOpacity: 0,
					lat: 0,
					lng: 0,
				},
			});
			// Der BindingMode des Models wird auf TwoWay gesetzt, damit der View die Werte des Models anpassen kann (z.B. das Ziehen des Ziel-Markers aktualisiert das Model)
			model.setDefaultBindingMode('TwoWay');

			// Füge das erstellte Model dem View mit dem Namen 'data' hinzu.
			// Nach diesem Aufruf sind alle Daten des Models in diesem View per DataBinding verfügbar: z.B. {data>/position/lat}
			view.setModel(model, 'data');

			// starte die User-Positions-Update-Routine
			this.updateCurrentGeoLocation();
		},

		/**
		 * Event-Handler: Wird ausgeführt, wenn der Positionsmarker des Benutzers geklickt wird
		 * @param oEvent
		 */
		onMarkerPress: function (oEvent) {
			// Hole die Position des Markers aus den Event-Parametern
			var latlng = oEvent.getParameter('latlng');

			// Zentriere die Karte über dem Marker
			this.map.setCenterFromLatLng(latlng, false);
			// Zoome die Karte auf Zoomstufe 15
			this.map.setZoom(15);
		},

		/**
		 * Event-Handler: Wird ausgeführt, wenn der Button im Header der Seite geklickt wird, um eine neue Tour zu beginnen
		 * @param oEvent
		 */
		onBtnNewTourPress: function (oEvent) {
			// Hole das data-Model aus dem View
			var oView = this.getView();
			var oDataModel = oView.getModel('data');

			// Hole die Zielmarkersichtbarkeit aus dem data-Model
			var iDestinationMarkerOpacity = oDataModel.getProperty('/destination/markerOpacity');

			if (iDestinationMarkerOpacity !== 0) {
				// wenn die Sichtbarkeit einem Wert ungleich 0 entspricht, dann setze die Sichtbarkeit auf 0
				oDataModel.setProperty('/destination/markerOpacity', 0);
			} else {
				// andernfalls setze die Sichtbarkeit auf 1 und die Position des Zielmarkers auf die aktuelle Position des Users
				var oUserPosition = oDataModel.getProperty('/position');

				oDataModel.setProperty('/destination', {
					markerOpacity: 1,
					lat: oUserPosition.lat,
					lng: oUserPosition.lng,
				});
			}

		},

		/**
		 * Event-Handler: Wird ausgeführt, wenn der Button im Popup zum Buchen eines Taxis geklickt wurde
		 * @param oEvent
		 */
		onBtnBookTaxiPress: function (oEvent) {
			// bestimme den Marker: Der Marker ist das Elterncontrol des gedrückten Buttons
			var oSource = oEvent.getSource();
			var oMarker = oSource.getParent();

			// Schließe das aktuell geöffnete Popup
			oMarker.closePopup();

			// Hole das data-Model aus dem View
			var oView = this.getView();
			var oDataModel = oView.getModel('data');

			// Hole aktuelle und Zielposition aus dem Model
			var currentPosition = oDataModel.getProperty('/position');
			var destination = oDataModel.getProperty('/destination');

			// Buche das Taxi
			TaxiService.bookTaxi(currentPosition, destination);
		},

		/**
		 * Event-Handler: Wird ausgeführt, wenn der Button im Popup zum Abbrechen der Routenplanung gedrückt wurde
		 * @param oEvent
		 */
		onBtnBookCancelPress: function (oEvent) {
			// bestimme den Marker: Der Marker ist das Elterncontrol des gedrückten Buttons
			var oSource = oEvent.getSource();
			var oMarker = oSource.getParent();

			// Schließe das aktuell geöffnete Popup
			oMarker.closePopup();

			// Hole das data-Model aus dem View
			var oView = this.getView();
			var oDataModel = oView.getModel('data');

			// setze die Koordinaten des Zielmarkers, sowie dessen Sichtbarkeit auf 0
			oDataModel.setProperty('/destination', {
				markerOpacity: 0,
				lat: 0,
				lng: 0,
			});
		},

		/**
		 * Startet die Updateroutine der User-Position
		 */
		updateCurrentGeoLocation: function () {
			// hole das data-Model, dass in der 'onInit'-Methode erstellt wurde, aus dem View
			var view = this.getView();
			var model = view.getModel('data');

			// abonniere alle Änderungen der aktuellen Position des Benutzers
			navigator.geolocation.watchPosition(function(position) {
				// Get the coordinates of the current possition.
				var lat = position.coords.latitude;
				var lng = position.coords.longitude;

				// Aktualisiere die Position des Users im data-Model mit den neuen Werten
				model.setProperty('/position', {
					lat: lat,
					lng: lng,
				});
			}, function (error) {
				// gebe alle Fehler in die Konsole des Browsers aus
				console.error(error.message)
			}, {
				// Aktiviere hohe Genauigkeit der Positionsbestimmung
				enableHighAccuracy: true,
			});
		},

	});
});