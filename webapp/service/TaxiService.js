sap.ui.define([
    "sap/ui/base/EventProvider",
], function(EventProvider) {
    "use strict";

    // erstelle einen neuen Socket.IO-Namespace und speichere die Instanz in der Variable 'socket'
    var socket = io.connect('/taxis');

    var TaxiService = EventProvider.extend("ortec.sommercamp.service.TaxiService", {

        // speichert alle Taxis, nach Name indeziert
        taxis: {},

        constructor: function () {
            // führe constructor der Superklasse EventProvider aus
            EventProvider.prototype.constructor.apply(this, arguments);

            // binde event handler
            this.handleTaxiUpdated = this.handleTaxiUpdated.bind(this);

            // rufe den status aller Taxis per HTTP-GET-Request vom Server ab
            fetch('/api/taxis')
                // wandle die Antwort in JSON um
                .then(function(response) { return response.json(); })
                // aktualisiere das taxis Objekt dieser Klasse mit der Antwort vom Server
                .then(function(json) {
                    this.taxis = json;
                }.bind(this));

            // lausche auf das 'taxi.updated'-Event des '/taxis'-Socket; wird ein solches Event vom Server ausgelöst, wird die Methode 'handleTaxiUpdated' ausgeführt.
            socket.on('taxi.updated', this.handleTaxiUpdated);
        },

        /**
         * Gibt allle zurzeit bekannten Taxis zurück
         * @returns {TaxiService.taxis|{}}
         */
        getTaxis: function () {
            return this.taxis;
        },

        /**
         * Bucht ein Taxi von einem bestimmten Startpunkt zu einem Zielpunkt
         *
         * @param start Startpunkt: Ein Objekt mit den numerischen Eigenschaften: 'lat' und 'lng'
         * @param destination Zielpunkt: Ein Objekt mit den numerischen Eigenschaften: 'lat' und 'lng'
         * @returns {Promise<any>}
         */
        bookTaxi: function (start, destination) {
            return fetch('/api/taxis', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    start: start,
                    destination: destination,
                }),
            })
                .then(function (response) { return response.json() })
        },

        /**
         * Fügt die den 'listener' (Funktion) dem Event 'taxi.updated' hinzu; wird dieses ausgelöst, werden alle Listener mit ihrem jeweiligem context ausgeführt.
         * @param listener Funktion, die beim Ereigniss ausgeführt werden soll
         * @param context Ein belibiges Objekt, meistens ergibt 'this' Sinn
         * @returns {*}
         */
        attachTaxiUpdated: function (listener, context) {
            return this.attachEvent('taxi.updated', listener, context);
        },

        /**
         * Fügt die den 'listener' (Funktion) dem Event 'taxi.updated' hinzu; wird dieses ausgelöst, werden alle Listener mit ihrem jeweiligem context ausgeführt.
         * @param listener Funktion, die beim Ereigniss ausgeführt werden soll
         * @param context Ein belibiges Objekt, meistens ergibt 'this' Sinn
         * @returns {*}
         */
        attachTaxisUpdated: function (listener, context) {
            return this.attachEvent('taxis.updated', listener, context);
        },

        /**
         *
         * @param event
         */
        handleTaxiUpdated: function (event) {
            // hole das Taxi-Objekt aus dem event-Objekt und speichere es in der Variable 'taxi'
            var taxi = event.taxi;

            // aktualisiere das Taxi im 'taxis' Objekt dieser Klasse
            this.taxis[taxi.name] = taxi;

            // löse die Ereignisse 'taxi.updated', 'taxis.updated' aus
            this.fireEvent('taxi.updated', event);
            this.fireEvent('taxis.updated', {
                taxis: this.taxis
            });
        },

    });

    return new TaxiService();

});