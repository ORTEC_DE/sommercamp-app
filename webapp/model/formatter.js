sap.ui.define([], function () {
	"use strict";

	return {

		/**
		 * Gibt beim Status 'free' die Farbe 'green' zurück; bei allen anderen Status 'red'
		 * @param sState {string} Der Status
		 * @returns {string} Die Farbe
		 */
		getTaxiColorFromState: function (sState) {
			if (sState === 'free') {
				return 'green';
			} else {
				return 'red';
			}
		},

	};
});