sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"../service/TaxiService",
], function(JSONModel, Device, TaxiService) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		/**
		 * Erstellt ein OneWay-Model, dass alle Taxis beinhaltet
		 * @returns Das Taxis-Model
		 */
		createTaxiModel: function () {
			// erstellt eine Instanz des JSONModels, dass alle Taxis beinhaltet
			var oModel = new JSONModel(TaxiService.getTaxis());
			// setzt den BindingMode des Models auf OneWay: Es werden keine Änderungen durch den View zugelassen
			oModel.setDefaultBindingMode("OneWay");

			// abonniere das 'taxis.updated' Ereignis des TaxiService
			TaxiService.attachTaxisUpdated(function (event) {
				// wird das Ereignis ausgelöst, hole alle Taxis aus dem Event-Parameter 'taxis'
				var taxis = event.getParameter("taxis");

				// überschreibe die Daten des Models mit den aktualisierten Taxis
				oModel.setData(taxis);
			});

			// gebe das erstellte Model zurück
			return oModel;
		}

	};
});