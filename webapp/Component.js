sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"./model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("ortec.sommercamp.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * Die Datei 'Component.js' wird durch UI5 währnd des Starts der App automatisch initialisiert und führt die 'init'-Methode aus
		 * @public
		 * @override
		 */
		init: function() {
			// führe init der Superklasse UIComponent aus
			UIComponent.prototype.init.apply(this, arguments);

			// Füge der Component das 'device'-Model hinzu.
			// Alle Views (mit ihren Controllern) die in dieser Component angezeigt werden, haben Zugriff auf dieses Model
			this.setModel(models.createDeviceModel(), "device");

			// Füge der Component das 'taxis'-Model hinzu.
			this.setModel(models.createTaxiModel(), "taxis");

			// erstelle die Views basierend auf der URL/Hash
			this.getRouter().initialize();
		}
	});
});